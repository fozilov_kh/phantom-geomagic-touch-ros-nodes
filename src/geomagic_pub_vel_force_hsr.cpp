#include "ros/ros.h"
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/WrenchStamped.h"
#include "sensor_msgs/JointState.h"
#include <HDU/hduVector.h>
#include "geomagic_control/DeviceButtonEvent.h"
#include "geomagic_control/DeviceFeedback.h"
#include "gazebo_msgs/LinkStates.h"




geometry_msgs::TwistStamped twist;
geometry_msgs::Pose pose;
geometry_msgs::PoseStamped pose3;
geomagic_control::DeviceFeedback force;
ros::Publisher force_pub;
ros::Publisher pose_pub;



void geomagic_callback(const geometry_msgs::Twist& geo_msg){
	twist.twist.linear.x = 0.001*geo_msg.linear.z;
    	twist.twist.linear.y = 0.001*geo_msg.linear.x;
    	twist.twist.linear.z = 0.001*geo_msg.linear.y;
	twist.twist.angular.x = 0.0;//geo_msg.angular.z;
        twist.twist.angular.y = 0.0;//geo_msg.angular.x;
        twist.twist.angular.z = 0.0;//geo_msg.angular.y;
}
void pose_callback(const geometry_msgs::PoseStamped& pose_msg){
	pose.position.x = 0.27082+0.001*(88.114+pose_msg.pose.position.z);
    	pose.position.y = 0.13435+0.001*pose_msg.pose.position.x;
    	pose.position.z = 0.15704+0.001*(65.510+pose_msg.pose.position.y);
	pose.orientation.x = pose_msg.pose.orientation.x;//geo_msg.angular.z;
        pose.orientation.y = pose_msg.pose.orientation.y;//geo_msg.angular.x;
        pose.orientation.z = pose_msg.pose.orientation.z;//geo_msg.angular.y;
	pose.orientation.w = pose_msg.pose.orientation.w;//geo_msg.angular.y;
	pose_pub.publish(pose);
	
}

void ur_callback(const geometry_msgs::WrenchStamped& ur_msg){
	force.force.x = 0*ur_msg.wrench.force.y;
	force.force.y = -ur_msg.wrench.force.z;
	force.force.z = 0*ur_msg.wrench.force.x;
}
//void button_callback(const geomagic_control::DeviceButtonEvent& button_msg){	

//	if(button_msg.grey_button){
	//force.force.x =-1.0;
	//force.force.y =-1.0;
	//force.force.z =-1.0;
	//force.lock.clear();
	//force.lock.push_back(0);
	//force.lock.push_back(0);
	//force.lock.push_back(0);
	//}
//	else{
	//force.force.x =1.0;
	//force.force.y =1.0;
	//force.force.z =1.0;
	//force.position.x =pose2.pose.position.x;
	//force.position.y =pose2.pose.position.y;
	//force.position.z =pose2.pose.position.z;
	//force.lock.clear();
	//force.lock.push_back(1);
	//force.lock.push_back(1);
	//force.lock.push_back(1);

	//}
	//force_pub.publish(force);
//}

int main(int argc, char **argv){
    ros::init(argc, argv, "geomagic_pub");
    ros::NodeHandle n;
	
    force.lock.push_back(0);//It doesn't work without this sentence.

    //publish
    ros::Publisher velocity_pub = n.advertise<geometry_msgs::TwistStamped>("velocity", 1);
    force_pub = n.advertise<geomagic_control::DeviceFeedback>("/force_feedback", 1);
    pose_pub = n.advertise<geometry_msgs::Pose>("/position", 1);
    //subscriibe
    ros::Subscriber geomagic_sub   = n.subscribe("/Geomagic/twist", 1, geomagic_callback);
    ros::Subscriber pose_sub   = n.subscribe("/Geomagic/pose", 1, pose_callback);
    ros::Subscriber ur_sub   = n.subscribe("/ur_control_imp/force_filtered", 1, ur_callback);
    //ros::Subscriber button_sub   = n.subscribe("/Geomagic/button", 1, button_callback);
	
    ros::Rate loop_rate(500);
    while (ros::ok()){
        velocity_pub.publish(twist);
	force_pub.publish(force);
    	ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}

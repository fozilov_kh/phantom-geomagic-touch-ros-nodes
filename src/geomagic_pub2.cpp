#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/WrenchStamped.h"
#include "sensor_msgs/JointState.h"
#include <HDU/hduVector.h>
#include "geomagic_control/DeviceButtonEvent.h"
#include "geomagic_control/DeviceFeedback.h"


geometry_msgs::PoseStamped pose;
geometry_msgs::PoseStamped pose2;
geometry_msgs::PoseStamped pose3;
geomagic_control::DeviceFeedback force;
ros::Publisher force_pub;



void geomagic_callback(const geometry_msgs::PoseStamped& geo_msg){
	pose.pose.position.x =0.4000+0.003*(88.114+geo_msg.pose.position.z);
    	pose.pose.position.y =0.2000+0.003*geo_msg.pose.position.x;
    	pose.pose.position.z =0.3000+0.003*(65.510+geo_msg.pose.position.y);
	pose.pose.orientation.x = geo_msg.pose.orientation.z;
        pose.pose.orientation.y = geo_msg.pose.orientation.x;
        pose.pose.orientation.z = geo_msg.pose.orientation.y;
        pose.pose.orientation.w = geo_msg.pose.orientation.w;
	pose2.pose.position.x =geo_msg.pose.position.x;
    	pose2.pose.position.y =geo_msg.pose.position.y;
    	pose2.pose.position.z =geo_msg.pose.position.z;
}
void ur_callback(const geometry_msgs::WrenchStamped& ur_msg){
	force.force.x =0.0001*ur_msg.wrench.force.z;
	force.force.y =0.0001*ur_msg.wrench.force.x;
	force.force.z =0.0001*ur_msg.wrench.force.y;
}

void button_callback(const geomagic_control::DeviceButtonEvent& button_msg){	

	if(button_msg.grey_button){
	//force.force.x =-1.0;//(1)-1N force works one direction.
	//force.force.y =-1.0;
	//force.force.z =-1.0;
	//force.lock.clear();//(2)Deveice is unlocked while releacing black button.
	//force.lock.push_back(0);
	//force.lock.push_back(0);
	//force.lock.push_back(0);
	}
	else{
	//force.force.x =1.0;//(1)1N force works one direction.
	//force.force.y =1.0;
	//force.force.z =1.0;
	//force.position.x =pose2.pose.position.x;//(2)Deveice is locked while pushing black button.
	//force.position.y =pose2.pose.position.y;
	//force.position.z =pose2.pose.position.z;
	//force.lock.clear();
	//force.lock.push_back(1);
	//force.lock.push_back(1);
	//force.lock.push_back(1);

	}
	//force_pub.publish(force);
}

int main(int argc, char **argv){
    ros::init(argc, argv, "geomagic_pub");
    ros::NodeHandle n;
	
	force.lock.push_back(0);//It doesn't work without this sentence.

    //publish
    ros::Publisher pose_pub = n.advertise<geometry_msgs::PoseStamped>("pose", 1);
    force_pub = n.advertise<geomagic_control::DeviceFeedback>("/force_feedback", 1);

    //subscriibe
    ros::Subscriber geomagic_sub   = n.subscribe("/Geomagic/pose", 1, geomagic_callback);
    ros::Subscriber ur_sub   = n.subscribe("/ftsensor/raw", 1, ur_callback);
    ros::Subscriber button_sub   = n.subscribe("/Geomagic/button", 1, button_callback);
	
    ros::Rate loop_rate(500);
    while (ros::ok()){
        pose.header.stamp = ros::Time::now();
        pose_pub.publish(pose);
	force_pub.publish(force);
    	ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}

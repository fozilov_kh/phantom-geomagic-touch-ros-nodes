#include "ros/ros.h"
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/WrenchStamped.h"
#include "sensor_msgs/JointState.h"
#include <HDU/hduVector.h>
#include "geomagic_control/DeviceButtonEvent.h"
#include "geomagic_control/DeviceFeedback.h"
#include "gazebo_msgs/LinkStates.h"




geometry_msgs::TwistStamped twist;
geometry_msgs::Pose pose;
geomagic_control::DeviceFeedback force;
geomagic_control::DeviceButtonEvent button;
ros::Publisher force_pub;
ros::Publisher pose_pub;
ros::Publisher button_pub;
ros::Publisher velocity_pub;


void geomagic_callback(const geometry_msgs::Twist& geo_msg){
	twist.twist.linear.x = 0.001*geo_msg.linear.z;
    	twist.twist.linear.y = 0.001*geo_msg.linear.x;
    	twist.twist.linear.z = 0.001*geo_msg.linear.y;
	twist.twist.angular.x = 0.0;//geo_msg.angular.z;
        twist.twist.angular.y = 0.0;//geo_msg.angular.x;
        twist.twist.angular.z = 0.0;//geo_msg.angular.y;
        velocity_pub.publish(twist);
}

void pose_callback(const geometry_msgs::PoseStamped::ConstPtr& pose_msg){

	//ROS_INFO("subscribe pose");
	pose.position.x = 0.13435+0.001*(-1.90858+pose_msg->pose.position.x);
    	pose.position.y = 0.25710+0.001*(-395.05+pose_msg->pose.position.y);
    	pose.position.z = 0.27082+0.001*(177.94+pose_msg->pose.position.z);
	pose.orientation.x = pose_msg->pose.orientation.x;
        pose.orientation.y = pose_msg->pose.orientation.y;
        pose.orientation.z = pose_msg->pose.orientation.z;
	pose.orientation.w = pose_msg->pose.orientation.w;

	pose_pub.publish(pose);

	
	
}

void hsr_force(const geometry_msgs::WrenchStamped::ConstPtr& msg){
	force.force.x = msg->wrench.force.x;
	force.force.y = msg->wrench.force.y;
	force.force.z = msg->wrench.force.z;
	force_pub.publish(force);
}


//void button_callback(const geomagic_control::DeviceButtonEventConstPtr& button_msg){

	//button = *button_msg;

	//button_pub.publish(button);

//}

int main(int argc, char **argv){
    ros::init(argc, argv, "phantom_pub");
    ros::NodeHandle n;
	
    force.lock.push_back(0);//It doesn't work without this sentence.

    //publish
    velocity_pub = n.advertise<geometry_msgs::TwistStamped>("velocity", 10);
    force_pub = n.advertise<geomagic_control::DeviceFeedback>("/force_feedback", 10);
    pose_pub = n.advertise<geometry_msgs::Pose>("/pose", 10);
    //button_pub = n.advertise<geomagic_control::DeviceButtonEvent>("/phantom/button", 1);
    //subscriibe
    ros::Subscriber geomagic_sub   = n.subscribe("/PHANToM/twist", 1, geomagic_callback,ros::TransportHints().reliable().tcpNoDelay(true));
    ros::Subscriber pose_sub   = n.subscribe("/PHANToM/pose", 1, pose_callback,ros::TransportHints().reliable().tcpNoDelay(true));
    ros::Subscriber hsr_force_sub = n.subscribe("/phantom/wrist_wrench", 1, hsr_force,ros::TransportHints().reliable().tcpNoDelay(true)); //("/phantom/wrist_wrench", 1, hsr_force);
    //ros::Subscriber ur_sub   = n.subscribe("/time_delay/delayed_us", 1, ur_callback);
    //ros::Subscriber button_sub   = n.subscribe("/PHANTOM/button", 1, button_callback);
	
    ros::Rate loop_rate(80);
    while (ros::ok()){


    	ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}

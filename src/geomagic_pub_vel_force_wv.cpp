#include "ros/ros.h"
#include <math.h>
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/WrenchStamped.h"
#include "sensor_msgs/JointState.h"
#include <HDU/hduVector.h>
#include "geomagic_control/DeviceButtonEvent.h"
#include "geomagic_control/DeviceFeedback.h"
#include "gazebo_msgs/LinkStates.h"
#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/Header.h"
#include "geomagic_control/master_control.h"

using namespace master_control;

  master::master(ros::NodeHandle &node_handle) {
  n = node_handle;
  force_pub = n.advertise<geomagic_control::DeviceFeedback>("/force_feedback", 1);
  u_m_pub = n.advertise<std_msgs::Float64MultiArray>("/u_m_trans", 1);
  velocity_pub = n.advertise<geometry_msgs::TwistStamped>("velocity", 1);
  pose_pub = n.advertise<geometry_msgs::Pose>("/position", 1);
  time_pub =n.advertise<std_msgs::Header>("/master/time",1);
  button_pub =n.advertise<std_msgs::Float64>("/gripper",1);
  geomagic_sub   = n.subscribe("/Geomagic/twist", 1, &master::geomagic_callback,this);
  //ur_sub   = n.subscribe("time_delay/delayed_us", 1, &master::ur_callback,this);
  ur_sub   = n.subscribe("/u_s_trans", 1, &master::ur_callback,this);
  //time_sub = n.subscribe("/time_delay/slave/time",1 ,&master::time_delay_callback,this);
  time_sub = n.subscribe("/slave/time",1 ,&master::time_delay_callback,this);
  pose_sub = n.subscribe("/Geomagic/pose", 1, &master::pose_callback,this);
  button_sub = n.subscribe("/Geomagic/button", 1, &master::button_callback,this);

  b_x_ = 30.0;
  b_y_ = 30.0;
  b_z_ = 30.0;
  double r_coef;
  r_coef=0.0909;
  u_m_.data.resize(3, 0);
  v_m_.data.resize(3, 0);
  wv_vm_f_filter = new f_filters::IIR(3, r_coef);
}

master::~master(){};

void master::geomagic_callback(const geometry_msgs::Twist& geo_msg){
	twist.twist.linear.x = 0.001*geo_msg.linear.z;
    	twist.twist.linear.y = 0.001*geo_msg.linear.x;
    	twist.twist.linear.z = 0.001*geo_msg.linear.y;
	twist.twist.angular.x = 0.0;//geo_msg.angular.z;
        twist.twist.angular.y = 0.0;//geo_msg.angular.x;
        twist.twist.angular.z = 0.0;//geo_msg.angular.y;
	velocity_pub.publish(twist);
	
}

void master::pose_callback(const geometry_msgs::PoseStamped& pose_msg){
	pose.position.x = 0.26006+0.001*(88.114+pose_msg.pose.position.z);
    	pose.position.y = 0.13469+0.001*pose_msg.pose.position.x;
    	pose.position.z = 0.25550+0.001*(65.510+pose_msg.pose.position.y);
	pose.orientation.x = pose_msg.pose.orientation.x;//geo_msg.angular.z;
        pose.orientation.y = pose_msg.pose.orientation.y;//geo_msg.angular.x;
        pose.orientation.z = pose_msg.pose.orientation.z;//geo_msg.angular.y;
	pose.orientation.w = pose_msg.pose.orientation.w;//geo_msg.angular.y;
	pose_pub.publish(pose);
	
}

void master::time_delay_callback(const std_msgs::Header& header_msg){
	std_msgs::Header now;
        now.stamp = ros::Time::now();
}

void master::ur_callback(const std_msgs::Float64MultiArray& u_s_){
        v_m_.data[0] = u_s_.data[0];
        v_m_.data[1] = u_s_.data[1];
	v_m_.data[2] = u_s_.data[2];
        v_m_tmp[0]=v_m_.data[0];
	v_m_tmp[1]=v_m_.data[1];
	v_m_tmp[2]=v_m_.data[2];
 	wv_vm_f_filter->update(v_m_tmp);
	v_m_tmp = wv_vm_f_filter->get_iir();
	v_m_.data[0] =v_m_tmp[0];
	v_m_.data[1] =v_m_tmp[1];
	v_m_.data[2] =v_m_tmp[2];
	
}

void master::button_callback(const geomagic_control::DeviceButtonEvent& button_msg){	
	if(button_msg.grey_button){
	gripper_angle_.data = 0.8;
	}
	else{
	gripper_angle_.data = 0.1;
	}
        //ROS_INFO_STREAM("gripper_angle_"<<gripper_angle_);
	button_pub.publish(gripper_angle_);
}
void master::init_force(){
	    force.lock.push_back(0);//It doesn't work without this sentence.
}

void master::set_force(){
        f_x =1* ((std::sqrt(2*b_x_))*v_m_.data[0] + b_x_*twist.twist.linear.x);
	f_y =1* ((std::sqrt(2*b_y_))*v_m_.data[1] + b_y_*twist.twist.linear.y);
	f_z =1* ((std::sqrt(2*b_z_))*v_m_.data[2] + b_z_*twist.twist.linear.z);
	//ROS_INFO_STREAM("v_m z: "<<v_m_.data[2]);
	//ROS_INFO_STREAM("f_z: "<<f_z);
        u_m_.data[0] = (1/std::sqrt(2*b_x_)) * (  f_x+  b_x_* twist.twist.linear.x);
        u_m_.data[1] = (1/std::sqrt(2*b_y_)) * (  f_y+  b_y_* twist.twist.linear.y);
        u_m_.data[2] = (1/std::sqrt(2*b_z_)) * (  f_z+  b_z_* twist.twist.linear.z);
	force.force.x = 0*-f_y; 
	force.force.y = 0*-f_z;
 	force.force.z = 0*f_x;
        time.stamp =ros::Time::now();
        time_pub.publish(time);
	u_m_pub.publish(u_m_);
	force_pub.publish(force);
}

int main(int argc, char **argv){
    ros::init(argc, argv, "geomagic_pub");
    ros::NodeHandle node_handle;
    ros::Rate loop_rate(500);
    master master(node_handle);
    master.init_force();
    while (ros::ok()){   
        master.set_force(); 
    	ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}

#include "ros/ros.h"
#include <math.h>
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/WrenchStamped.h"
#include "sensor_msgs/JointState.h"
#include <HDU/hduVector.h>
#include "geomagic_control/DeviceButtonEvent.h"
#include "geomagic_control/DeviceFeedback.h"
#include "gazebo_msgs/LinkStates.h"
#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/Header.h"
#include "geomagic_control/master_control_int.h"

using namespace master_control;

  master::master(ros::NodeHandle &node_handle) {
  n = node_handle;
  force_pub = n.advertise<geomagic_control::DeviceFeedback>("/force_feedback", 1);
  u_m_pub = n.advertise<std_msgs::Float64MultiArray>("/u_m_trans", 1);
  velocity_pub = n.advertise<geometry_msgs::TwistStamped>("velocity", 1);
  pose_pub = n.advertise<geometry_msgs::Pose>("/position", 1);
  time_pub =n.advertise<std_msgs::Header>("/master/time",1);
  geomagic_sub   = n.subscribe("/Geomagic/twist", 1, &master::geomagic_callback,this);
  ur_sub   = n.subscribe("u_s_trans", 1, &master::ur_int_callback,this);
  //ur_sub   = n.subscribe("time_delay/delayed_us", 1, &master::ur_int_callback,this);
  pose_sub = n.subscribe("/Geomagic/pose", 1, &master::pose_callback,this);
  time_sub = n.subscribe("/slave/time",1 ,&master::time_delay_callback,this);
  //time_sub = n.subscribe("/time_delay/slave/time",1 ,&master::time_delay_callback,this);

  b_=30.0;
  double r_coef;
  r_coef=0.0909;//0.03846;
  cf=100;
  u_m_trans.data.resize(3,0);
  v_m_int = Eigen::Vector3d::Zero();
  v_m_ = Eigen::Vector3d::Zero();
  wv_vm_f_filter = new f_filters::IIR(3, r_coef);
  wv_um_integrator = new integrators::integrators();
  wv_vm_integrator = new integrators::integrators();
}

master::~master(){};

void master::geomagic_callback(const geometry_msgs::Twist& geo_msg){
	twist.twist.linear.x = 0.001*geo_msg.linear.z;
    	twist.twist.linear.y = 0.001*geo_msg.linear.x;
    	twist.twist.linear.z = 0.001*geo_msg.linear.y;
	twist.twist.angular.x = 0.0;//geo_msg.angular.z;
        twist.twist.angular.y = 0.0;//geo_msg.angular.x;
        twist.twist.angular.z = 0.0;//geo_msg.angular.y;
	velocity_pub.publish(twist);

}

void master::pose_callback(const geometry_msgs::PoseStamped& pose_msg){
	pose.position.x = 0.26006+0.001*(88.114+pose_msg.pose.position.z);
    	pose.position.y = 0.13469+0.001*pose_msg.pose.position.x;
    	pose.position.z = 0.25442+0.001*(65.510+pose_msg.pose.position.y);
	pose.orientation.x = pose_msg.pose.orientation.x;//geo_msg.angular.z;
        pose.orientation.y = pose_msg.pose.orientation.y;//geo_msg.angular.x;
        pose.orientation.z = pose_msg.pose.orientation.z;//geo_msg.angular.y;
	pose.orientation.w = pose_msg.pose.orientation.w;//geo_msg.angular.y;
	pose_pub.publish(pose);
	
}

void master::time_delay_callback(const std_msgs::Header& header_msg){
	std_msgs::Header now;
        now.stamp = ros::Time::now();
	ROS_INFO_STREAM("time_delay:"<<now.stamp-header_msg.stamp);
}

void master::ur_int_callback(const std_msgs::Float64MultiArray& u_s_trans){
	v_m_[0] = u_s_trans.data[0];
        v_m_[1] = u_s_trans.data[1];
	v_m_[2] = u_s_trans.data[2];
	//ROS_INFO_STREAM("v_m_[2]  "<<v_m_[2]);
        v_m_tmp[0]=v_m_[0];
	v_m_tmp[1]=v_m_[1];
	v_m_tmp[2]=v_m_[2];
 	wv_vm_f_filter->update(v_m_tmp);
	v_m_tmp = wv_vm_f_filter->get_iir();
	v_m_[0] =v_m_tmp[0];
	v_m_[1] =v_m_tmp[1];
	v_m_[2] =v_m_tmp[2];
        //v_m_trans[0] = u_s_trans.data[0];//v_m(t-T)=u_s(t)
        //v_m_trans[1] = u_s_trans.data[1];
	//v_m_trans[2] = u_s_trans.data[2];
	//v_m_[0] =  cf*(v_m_trans[0]-v_m_int[0]);
	//v_m_[1] =  cf*(v_m_trans[1]-v_m_int[1]);
	//v_m_[2] =  cf*(v_m_trans[2]-v_m_int[2]);
	//v_m_int[0]=v_m_[0];//v_m_integrator
	//v_m_int[1]=v_m_[1];
	//v_m_int[2]=v_m_[2];
	//wv_vm_integrator->update(v_m_int);
	//v_m_int = wv_vm_integrator->integrate();
	//ROS_INFO_STREAM("v_m z: "<<v_m_[2]);
}


void master::init_force(){
	    force.lock.push_back(0);//It doesn't work without this sentence.
}

void master::set_force(){
	f_x = 1* ((std::sqrt(2*b_))*v_m_[0] + b_*twist.twist.linear.x);//f_m=√(2b)v_m+bx_m
	f_y = 1* ((std::sqrt(2*b_))*v_m_[1] + b_*twist.twist.linear.y);
	f_z = 1* ((std::sqrt(2*b_))*v_m_[2] + b_*twist.twist.linear.z);
	//ROS_INFO_STREAM("f_z: "<<f_z);
        u_m_[0] =     (1/std::sqrt(2*b_)) * ( f_x+  b_* twist.twist.linear.x);//u_m=(1/√(2b))(f_m+bx_m)
        u_m_[1] =     (1/std::sqrt(2*b_)) * ( f_y+  b_* twist.twist.linear.y);
        u_m_[2] =     (1/std::sqrt(2*b_)) * ( f_z+  b_* twist.twist.linear.z);
	//ROS_INFO_STREAM("u_m_: "<<u_m_[2]);
        u_m_int[0]=u_m_[0];//u_m_integrator
	u_m_int[1]=u_m_[1];
	u_m_int[2]=u_m_[2];
	wv_um_integrator->update(u_m_int);
	u_m_int = wv_um_integrator->integrate();
	//ROS_INFO_STREAM("u_m_int: "<<u_m_int[2]);
	u_m_trans.data[0] = u_m_int[0]+(1/cf)*u_m_[0];
	u_m_trans.data[1] = u_m_int[1]+(1/cf)*u_m_[1];
	u_m_trans.data[2] = u_m_int[2]+(1/cf)*u_m_[2];
	//u_m_trans.data[0] = u_m_[0];
	//u_m_trans.data[1] = u_m_[1];
	//u_m_trans.data[2] = u_m_[2];
	force.force.x = 0; //match axis  
	force.force.y = -f_z;
 	force.force.z = 0;
        time.stamp =ros::Time::now();
        time_pub.publish(time);
	u_m_pub.publish(u_m_trans);
	force_pub.publish(force);
}

int main(int argc, char **argv){
    ros::init(argc, argv, "geomagic_pub");
    ros::NodeHandle node_handle;
    ros::Rate loop_rate(500);
    master master(node_handle);
    master.init_force();

    while (ros::ok()){  
        master.set_force();  
    	ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}

#include "ros/ros.h"
#include <math.h>
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/WrenchStamped.h"
#include "sensor_msgs/JointState.h"
#include <HDU/hduVector.h>
#include "geomagic_control/DeviceButtonEvent.h"
#include "geomagic_control/DeviceFeedback.h"
#include "gazebo_msgs/LinkStates.h"
#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/Float64.h"
#include "std_msgs/Header.h"
#include "geomagic_control/master_control.h"

using namespace master_control;

  master::master(ros::NodeHandle &node_handle) {
  n = node_handle;
  velocity_pub = n.advertise<geometry_msgs::TwistStamped>("velocity", 1);
  pose_pub = n.advertise<geometry_msgs::PoseStamped>("position", 1);
  pinch_pub = n.advertise<std_msgs::Float64>("/gripper",1);
  //button_pub =n.advertise<std_msgs::Float64>("/gripper",1);
  geomagic_sub   = n.subscribe("/PHANToM/twist", 1, &master::geomagic_callback,this);
  pose_sub = n.subscribe("/PHANToM/pose", 1, &master::pose_callback,this);
  //button_sub = n.subscribe("/PHANToM/button", 1, &master::button_callback,this);
  pinch_sub = n.subscribe("/PHANToM/pinch", 1, &master::pinch_callback,this);

  b_x_=30.0;
  b_y_=25.0;
  b_z_=30.0;
  double r_coef;
  r_coef=0.0909;

  wv_um_integrator = new integrators::integrators();
}

master::~master(){};

void master::geomagic_callback(const geometry_msgs::Twist& geo_msg){
	twist.header.stamp = ros::Time::now();
	twist.twist.linear.x = -0.001*geo_msg.linear.z;
    	twist.twist.linear.y = -0.001*geo_msg.linear.x;
    	twist.twist.linear.z = -0.001*geo_msg.linear.y;
	ang_vel_tmp[0] = geo_msg.angular.z;
	ang_vel_tmp[1] = geo_msg.angular.x;
	ang_vel_tmp[2] = geo_msg.angular.y;
	//if(ang_vel_tmp[0]<-1||ang_vel_tmp[0]>1)ang_vel_tmp[0]=0;
	//if(ang_vel_tmp[1]<-1||ang_vel_tmp[1]>1)ang_vel_tmp[1]=0;
	//if(ang_vel_tmp[2]<-1||ang_vel_tmp[2]>1)ang_vel_tmp[2]=0;
	twist.twist.angular.x = ang_vel_tmp[0];
        twist.twist.angular.y = ang_vel_tmp[1];
        twist.twist.angular.z = ang_vel_tmp[2];

}



void master::pose_callback(const geometry_msgs::PoseStamped& pose_msg){
	posestamped.pose.position.x = 0.26006+0.001*( 178.037+pose_msg.pose.position.z);
    	posestamped.pose.position.y = 0.13469+0.001*(-1.90858+pose_msg.pose.position.x);
    	posestamped.pose.position.z = 0.25550+0.001*(-395.079+pose_msg.pose.position.y);
	posestamped.pose.orientation.x = pose_msg.pose.orientation.x;
        posestamped.pose.orientation.y = pose_msg.pose.orientation.y;
        posestamped.pose.orientation.z = pose_msg.pose.orientation.z;
	posestamped.pose.orientation.w = pose_msg.pose.orientation.w;
	pose_pub.publish(posestamped);
	
}
void master::pinch_callback(const std_msgs::Float64& pinch_msg){
	gripper_angle_.data = 0.8 - 0.7*pinch_msg.data;
	pinch_pub.publish(gripper_angle_);
}

//void master::button_callback(const geomagic_control::DeviceButtonEvent& button_msg){	
//	if(button_msg.grey_button){
//	gripper_angle_.data = 0.8;
//	}
//	else{
//	gripper_angle_.data = 0.1;
//	}
        //ROS_INFO_STREAM("gripper_angle_"<<gripper_angle_);
//	button_pub.publish(gripper_angle_);
//}

void master::init_force(){
	force.lock.push_back(0);//It doesn't work without this sentence.
}

void master::set_force(){
	velocity_pub.publish(twist);
}


int main(int argc, char **argv){
    ros::init(argc, argv, "geomagic_pub");
    ros::NodeHandle node_handle;
    ros::Rate loop_rate(500);
    master master(node_handle);
    master.init_force();
    while (ros::ok()){  
	master.set_force();  
    	ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}

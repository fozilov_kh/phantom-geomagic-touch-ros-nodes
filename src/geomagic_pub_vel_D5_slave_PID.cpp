#include "ros/ros.h"
#include <math.h>
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/WrenchStamped.h"
#include "sensor_msgs/JointState.h"
#include <HDU/hduVector.h>
#include "geomagic_control/DeviceButtonEvent.h"
#include "geomagic_control/DeviceFeedback.h"
#include "gazebo_msgs/LinkStates.h"
#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/Float64.h"
#include "std_msgs/Header.h"
#include "geomagic_control/master_control.h"
#include <geometry_msgs/Vector3.h>
#include <control_msgs/JointTrajectoryControllerState.h>
	

using namespace master_control;
master::master(ros::NodeHandle &node_handle)
{
	n = node_handle;
	slave_error_pub = n.advertise<geometry_msgs::Pose>("/slave/error", 1);
	slave_error_before_pub = n.advertise<geometry_msgs::Pose>("/slave/error_before", 1);
	velocity_pub = n.advertise<geometry_msgs::TwistStamped>("velocity", 1);
	pose_pub = n.advertise<geometry_msgs::PoseStamped>("/slave/position", 1);
	button_pub = n.advertise<std_msgs::Float64MultiArray>("/button", 1);
	force_pub = n.advertise<geomagic_control::DeviceFeedback>("/slave/force_feedback", 1);
	//force2_pub = n.advertise<geomagic_control::DeviceFeedback>("/slave/force_feedback_real", 1);
	
	geomagic_sub = n.subscribe("Geomagic_slave/twist", 1, &master::geomagic_callback, this);
	pose_sub = n.subscribe("Geomagic_slave/pose", 1, &master::pose_callback, this);
	button_sub = n.subscribe("Geomagic_slave/button", 1, &master::button_callback, this);
	joint_sub = n.subscribe("Geomagic_slave/joint_states", 1, &master::joint_callback, this);
	master_position_sub = n.subscribe("/master/position", 1, &master::master_position_Callback, this);
	// HandPosition_sub = n.subscribe("/nir/hand_pose/actual", 1, &master::HandPositionCallback, this);
	// HandVelocity_sub = n.subscribe("teleoperation_velocity", 1, &master::HandVelocityCallback, this);
	// RealAngle_sub = n.subscribe("/hsrb/omni_base_controller/state", 1, &master::RealAngleCallback, this);
	// Oculus_nensei_sub = n.subscribe("/phantom_nensei_topic", 1, &master::Oculus_nensei_Callback, this);
	double r_coef;
	r_coef = 0.0456;
	ang_vel_f_filter = new f_filters::IIR(3, r_coef);
	wv_um_integrator = new integrators::integrators();
	button.data.resize(2, 0);
	joint_position = Eigen::VectorXd::Zero(6);
	joint_velocity = Eigen::VectorXd::Zero(6);
	last_joint_position = Eigen::VectorXd::Zero(6);
	oculus_nensei = 0;
	past_position_error_x =0;
	past_position_error_y=0;
	past_position_error_z=0;
	now_position_error_x=0;
	now_position_error_y=0;
	now_position_error_z=0;

	double damp = 0.001;
	KP_x = 0.2;
	KI_x = 0.0;
	KD_x = damp;//0.0005
	KP_y = 0.2;
	KI_y = 0.0;
	KD_y = damp;
	KP_z = 0.2;
	KI_z = 0.0;
	KD_z = damp;
	DELTA_T = 0.002;
	integral_x=0;
	integral_y=0;
	integral_z=0;
	objective_position_x = 0.0;
	objective_position_y = 0.0;
	objective_position_z = 0.0;
	time_integral = 0.0;
	Hz = 0.25;
	Hz_2 = 1;
	//data_size = 10;
	force_x[10];
	force_y[10];
	force_z[10];
	
	force_result_x=0;
	force_result_y=0;
	force_result_z=0;




}

master::~master(){};
//sokudo masuta
void master::geomagic_callback(const geometry_msgs::Twist &geo_msg)
{
	twist.header.stamp = ros::Time::now();
	twist.twist.linear.x = -0.001 * geo_msg.linear.z;
	twist.twist.linear.y = -0.001 * geo_msg.linear.x;
	twist.twist.linear.z = 0.001 * geo_msg.linear.y;
	ang_vel_tmp[0] = geo_msg.angular.z;
	ang_vel_tmp[1] = geo_msg.angular.x;
	ang_vel_tmp[2] = geo_msg.angular.y;
	if (ang_vel_tmp[0] < -10 || ang_vel_tmp[0] > 10)
		ang_vel_tmp[0] = 0;
	if (ang_vel_tmp[1] < -10 || ang_vel_tmp[1] > 10)
		ang_vel_tmp[1] = 0;
	if (ang_vel_tmp[2] < -10 || ang_vel_tmp[2] > 10)
		ang_vel_tmp[2] = 0;
	ang_vel_f_filter->update(ang_vel_tmp);
	ang_vel_tmp = ang_vel_f_filter->get_iir();
	twist.twist.angular.x = ang_vel_tmp[0];
	twist.twist.angular.y = ang_vel_tmp[1];
	twist.twist.angular.z = ang_vel_tmp[2];
}

void master::joint_callback(const sensor_msgs::JointState &joint_msg)
{
	joint_position[0] = joint_msg.position[0];
	joint_position[1] = joint_msg.position[1];
	joint_position[2] = joint_msg.position[2];
	joint_position[3] = joint_msg.position[3];
	joint_position[4] = joint_msg.position[4];
	joint_position[5] = joint_msg.position[5];
	joint_velocity = (joint_position - last_joint_position) * 1000;
	last_joint_position = joint_position;
	//ROS_INFO_STREAM(""<<twist.twist.angular.x);
	//twist.twist.angular.x = joint_velocity[2];
	//twist.twist.angular.y = joint_velocity[1];
	//twist.twist.angular.z = joint_velocity[0];
}
//sireiti()
void master::pose_callback(const geometry_msgs::PoseStamped &pose_msg)
{
	posestamped.header.stamp = ros::Time::now();
	// posestamped.pose.position.x = 0.001 * (88.114 + pose_msg.pose.position.z);
	// posestamped.pose.position.y = 0.001 * pose_msg.pose.position.x;
	// posestamped.pose.position.z = 0.001 * (65.510 + pose_msg.pose.position.y);
	// posestamped.header.stamp = ros::Time::now();
	posestamped.pose.position.x = pose_msg.pose.position.x;
	// posestamped.pose.position.y = 0.001 * pose_msg.pose.position.x;
	// posestamped.pose.position.z = 0.001 * (65.510 + pose_msg.pose.position.y); 
	posestamped.pose.position.y = pose_msg.pose.position.y;
	posestamped.pose.position.z = pose_msg.pose.position.z;
	posestamped.pose.orientation.x = -pose_msg.pose.orientation.z;
	posestamped.pose.orientation.y = -pose_msg.pose.orientation.x;
	posestamped.pose.orientation.z = pose_msg.pose.orientation.y;
	posestamped.pose.orientation.w = pose_msg.pose.orientation.w;
	pose_pub.publish(posestamped);

	time_integral += DELTA_T;




	// objective_position_x = 50*sin(2*3.14159265358979*time_integral*Hz) + 10*sin(2*3.14159265358979*time_integral*Hz_2);
	// objective_position_y = 50*cos(2*3.14159265358979*time_integral*Hz) + 10*cos(2*3.14159265358979*time_integral*Hz_2);
	// objective_position_z = 20;



	//PID
	now_position_error_x = objective_position_x - pose_msg.pose.position.x;
	now_position_error_y = objective_position_y - pose_msg.pose.position.y;
	now_position_error_z = objective_position_z - pose_msg.pose.position.z;
	
	slave_error_before.position.x = now_position_error_x;
	slave_error_before.position.y = now_position_error_y;
	slave_error_before.position.z = now_position_error_z;
	slave_error.position.x = now_position_error_x;
	slave_error.position.y = now_position_error_y;
	slave_error.position.z = now_position_error_z;

	
	slave_error_before_pub.publish(slave_error_before);

// 	for (int i = 1; i < 10; i++) {
//         force_x[i-1] = force_x[i];
//         force_y[i-1] = force_y[i];
//         force_z[i-1] = force_z[i];
//     }
// 	force_x[9] = now_position_error_x;
// 	force_y[9] = now_position_error_y;
// 	force_z[9] = now_position_error_z;
// 	for (int i = 0; i < 10; i++) {
//         force_result_x += force_x[i];
//         force_result_y += force_y[i];
//         force_result_z += force_z[i];
//     }
// 	// slave_error.position.x = force_result_x / 10;
// 	// slave_error.position.y = force_result_y / 10;
// 	// slave_error.position.z = force_result_z / 10;

// 	ave_slave_error.position.x = force_result_x / 10;
// 	ave_slave_error.position.y = force_result_y / 10;
// 	ave_slave_error.position.z = force_result_z / 10;


//   double T = 0.0333333;
//   double w = 0.7;
//   slave_error.position.x = T * w * (ave_slave_error.position.x + pre_ave_slave_error.position.x) / (2 + T * w) + (2 - T * w) * pre_filter_slave_error.position.x / (2 + T * w);
//   slave_error.position.y = T * w * (ave_slave_error.position.y + pre_ave_slave_error.position.y) / (2 + T * w) + (2 - T * w) * pre_filter_slave_error.position.y / (2 + T * w);
//   slave_error.position.z = T * w * (ave_slave_error.position.z + pre_ave_slave_error.position.z) / (2 + T * w) + (2 - T * w) * pre_filter_slave_error.position.z / (2 + T * w);
//   pre_ave_slave_error = ave_slave_error;
//   pre_filter_slave_error = slave_error;


// 	force_result_x = 0;
// 	force_result_y = 0;
// 	force_result_z = 0;
    

// 	// slave_error.position.x = now_position_error_x;
// 	// slave_error.position.y = now_position_error_y;
// 	// slave_error.position.z = now_position_error_z;

 	slave_error_pub.publish(slave_error);

	integral_x += ( now_position_error_x+ past_position_error_x) / 2.0 * DELTA_T;
	integral_y += ( now_position_error_y+ past_position_error_y) / 2.0 * DELTA_T;
	integral_z += ( now_position_error_z+ past_position_error_z) / 2.0 * DELTA_T;

	// force.force.x = KP_x*now_position_error_x + KI_x*integral_x +  KD_x * (now_position_error_x-past_position_error_x)/DELTA_T;
	// force.force.y = KP_y*now_position_error_y + KI_y*integral_y +  KD_y * (now_position_error_y-past_position_error_y)/DELTA_T + 0.57;
	// force.force.z = KP_z*now_position_error_z + KI_z*integral_z +  KD_z * (now_position_error_z-past_position_error_z)/DELTA_T;

   



	force2.force.x = KP_x*now_position_error_x + KI_x*integral_x +  KD_x * (now_position_error_x-past_position_error_x)/DELTA_T;
	force2.force.y = KP_y*now_position_error_y + KI_y*integral_y +  KD_y * (now_position_error_y-past_position_error_y)/DELTA_T + 0.57;
	force2.force.z = KP_z*now_position_error_z + KI_z*integral_z +  KD_z * (now_position_error_z-past_position_error_z)/DELTA_T;
	
	int a = 1;


//移動平均ふぃるた
	for (int i = 1; i < a; i++) {
        force_x[i-1] = force_x[i];
        force_y[i-1] = force_y[i];
        force_z[i-1] = force_z[i];
    }

	force_x[a-1] = force2.force.x;
	force_y[a-1] = force2.force.y;
	force_z[a-1] = force2.force.z;

	for (int i = 0; i < a; i++) {
        force_result_x += force_x[i];
        force_result_y += force_y[i];
        force_result_z += force_z[i];
    }

    force.force.x = force_result_x / a;
	force.force.y = force_result_y / a;
	force.force.z = force_result_z / a;
	force_result_x = 0;
	force_result_y = 0;
	force_result_z = 0;

	// force=ave_force;
	// force = force2;



//   double T = 0.002;
//   double w = 1;
//   force.force.x = T * w * (ave_force.force.x + pre_ave_force.force.x) / (2 + T * w) + (2 - T * w) * pre_filter_force.force.x / (2 + T * w);
//   force.force.y = T * w * (ave_force.force.y + pre_ave_force.force.y) / (2 + T * w) + (2 - T * w) * pre_filter_force.force.y / (2 + T * w);
//   force.force.z = T * w * (ave_force.force.z + pre_ave_force.force.z) / (2 + T * w) + (2 - T * w) * pre_filter_force.force.z / (2 + T * w);
//   pre_ave_force = ave_force;
//   pre_filter_force = force;

	if (force.force.x < -3)
	{
		force.force.x = -3;
	}
	else if (force.force.x > 3)
	{
		force.force.x = 3;
	}
	if (force.force.y < -3)
	{
		force.force.y = -3;
	}
	else if (force.force.y > 3)
	{
		force.force.y = 3;
	}
	if (force.force.z < -3)
	{
		force.force.z = -3;
	}
	else if (force.force.z > 3)
	{
		force.force.z = 3;
	}


	force_pub.publish(force);
	// force2_pub.publish(force2);

	past_position_error_x = now_position_error_x;
	past_position_error_y = now_position_error_y;
	past_position_error_z = now_position_error_z;


}

void master::master_position_Callback(const geometry_msgs::PoseStamped &msg)
{
    objective_position_x = msg.pose.position.x;
	objective_position_y = msg.pose.position.y;
	objective_position_z = msg.pose.position.z;
}

void master::button_callback(const geomagic_control::DeviceButtonEvent &button_msg)
{

	if (button_msg.grey_button)
	{
		button.data[0] = 0;
		oculus_nensei = 0;
	}
	else
	{
		button.data[0] = 1;
	}

	//ROS_INFO_STREAM(""<<button.data[0]);
	//ROS_INFO_STREAM("gripper_angle_"<<gripper_angle_);
	if (button_msg.white_button)
	{
		button.data[1] = 1;
	}
	else
	{
		button.data[1] = 0;
	}
	button_pub.publish(button);
}

// void master::ur_callback(const geometry_msgs::WrenchStamped& ur_msg){
// 	force.force.x = ur_msg.wrench.force.y;
// 	force.force.y = -ur_msg.wrench.force.z;
// 	force.force.z = -ur_msg.wrench.force.x;
// }
void master::init_force()
{
	force.lock.push_back(0); //It doesn't work without this sentence.
}

// void master::RealAngleCallback(const control_msgs::JointTrajectoryControllerState &msg)
// {
// 	realangle.data = msg.actual.positions[2];
// }
// void master::HandPositionCallback(const geometry_msgs::PoseStamped &msg)
// {
// 	double a1;
// 	double b1;
// 	realposition_x = msg.pose.position.x * cos(realangle.data) - msg.pose.position.y * sin(realangle.data);
// 	realposition_y = msg.pose.position.x * sin(realangle.data) + msg.pose.position.y * cos(realangle.data);

// 	a1 = 0.1 / abs(realposition_x - 0.55);
// 	b1 = 0.5 * pow(10, a1);
// 	//ROS_INFO("b:%f", b1);
// 	// b = T * w * (a + a_) / (2 + T * w) + (2 - T * w) * b_ / (2 + T * w);
// 	// a_ = a;
// 	// b_ = b;
// 	//HandPosition_Gain.data = b * 0.5;
// 	// if (b1 < 15)
// 	// {
// 	// 	HandPosition_Gain.data = b1;
// 	// }
// 	// else
// 	// {
// 	// 	HandPosition_Gain.data = 15;
// 	// }

// 	//velocity_pub.publish(twist);
// 	//HandPosition_Gain.data = msg.pose.position.x;
// }
// void master::Oculus_nensei_Callback(const std_msgs::Float64 &msg)
// {
// 	oculus_nensei = msg.data;
// }
//tikarafeedback
void master::HandVelocityCallback(const geometry_msgs::Vector3 &msg)
{

	// 	HandVelocity.x = msg.x;
	// 	HandVelocity.y = msg.y;
	// 	HandVelocity.z = msg.z;
}


void master::set_force()
{
	double x;
	double y;
	double z;

	double T = 0.002;
	double w = 25;
	//double b = 1.2;
	// ROS_INFO_STREAM("" << prev_filter_force);
	HandPosition_Gain.data = 2.5;
	real_force_x = twist.twist.linear.y * HandPosition_Gain.data;
	real_force_y = -twist.twist.linear.z * HandPosition_Gain.data;
	real_force_z = twist.twist.linear.x * HandPosition_Gain.data;
	filter_force_x = T * w * (real_force_x + prev_real_force_x) / (2 + T * w) + (2 - T * w) * prev_filter_force_x / (2 + T * w);
	filter_force_y = T * w * (real_force_y + prev_real_force_y) / (2 + T * w) + (2 - T * w) * prev_filter_force_y / (2 + T * w);
	filter_force_z = T * w * (real_force_z + prev_real_force_z) / (2 + T * w) + (2 - T * w) * prev_filter_force_z / (2 + T * w);
	prev_real_force_x = real_force_x;
	prev_filter_force_x = filter_force_x;
	prev_real_force_y = real_force_y;
	prev_filter_force_y = filter_force_y;
	prev_real_force_z = real_force_z;
	prev_filter_force_z = filter_force_z;
	// force.force.x = filter_force_x;
	// force.force.y = filter_force_y;
	// force.force.z = filter_force_z;
	// force.force.x = 1;
	// force.force.y = 0;
	// force.force.z = 0;
	// if (z > 2)
	// {
	// 	ROS_INFO("ifaaaa");
	// 	z = 2;
	// }
	// if (twist.twist.linear.x > 0)
	// {
	// 	force.force.z = filter_force;
	// }
	// else
	// {
	// 	force.force.z = 0;
	// }

	// if (oculus_nensei == 1)
	// {

	// 	force_pub.publish(force);
	// }
	//force_pub.publish(force);
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "geomagic_pub_slave");
	ros::NodeHandle node_handle;
	ros::Rate loop_rate(500);
	master master(node_handle);
	master.init_force();
	while (ros::ok())
	{
		master.set_force();
		ros::spinOnce();
		loop_rate.sleep();
	}
	return 0;
}

#include "ros/ros.h"
#include <math.h>
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/WrenchStamped.h"
#include "sensor_msgs/JointState.h"
#include <HDU/hduVector.h>
#include "geomagic_control/DeviceButtonEvent.h"
#include "geomagic_control/DeviceFeedback.h"
#include "gazebo_msgs/LinkStates.h"
#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/Float64.h"
#include "std_msgs/Header.h"
#include "geomagic_control/master_control.h"

using namespace master_control;
  master::master(ros::NodeHandle &node_handle) {
  n = node_handle;
  velocity_pub = n.advertise<geometry_msgs::TwistStamped>("velocity", 1);
  pose_pub = n.advertise<geometry_msgs::PoseStamped>("/position", 1);
  button_pub = n.advertise<std_msgs::Float64MultiArray>("/button",1);
  force_pub = n.advertise<geomagic_control::DeviceFeedback>("/force_feedback", 1);
  geomagic_sub   = n.subscribe("/Geomagic/twist", 1, &master::geomagic_callback,this);
  pose_sub = n.subscribe("/Geomagic/pose", 1, &master::pose_callback,this);
  button_sub = n.subscribe("/Geomagic/button", 1, &master::button_callback,this);
  joint_sub = n.subscribe("/Geomagic/joint_states", 1, &master::joint_callback,this);
  double r_coef;
  r_coef=0.0456;
  ang_vel_f_filter = new f_filters::IIR(3, r_coef);
  wv_um_integrator = new integrators::integrators();
  button.data.resize(2 ,0);
  joint_position = Eigen::VectorXd::Zero(6);
  joint_velocity = Eigen::VectorXd::Zero(6);
  last_joint_position = Eigen::VectorXd::Zero(6);

}

master::~master(){};
//sokudo masuta
void master::geomagic_callback(const geometry_msgs::Twist& geo_msg){
	twist.header.stamp = ros::Time::now();
	twist.twist.linear.x = -0.001*geo_msg.linear.z;
    	twist.twist.linear.y = -0.001*geo_msg.linear.x;
    	twist.twist.linear.z = 0.001*geo_msg.linear.y;
	ang_vel_tmp[0] = geo_msg.angular.z;
	ang_vel_tmp[1] = geo_msg.angular.x;
	ang_vel_tmp[2] = geo_msg.angular.y;
	if(ang_vel_tmp[0]<-10||ang_vel_tmp[0]>10)ang_vel_tmp[0]=0;
	if(ang_vel_tmp[1]<-10||ang_vel_tmp[1]>10)ang_vel_tmp[1]=0;
	if(ang_vel_tmp[2]<-10||ang_vel_tmp[2]>10)ang_vel_tmp[2]=0;
        ang_vel_f_filter->update(ang_vel_tmp);
        ang_vel_tmp = ang_vel_f_filter->get_iir();
	twist.twist.angular.x = ang_vel_tmp[0];
        twist.twist.angular.y = ang_vel_tmp[1];
        twist.twist.angular.z = ang_vel_tmp[2];

}

void master::joint_callback(const sensor_msgs::JointState& joint_msg){
	joint_position[0] = joint_msg.position[0];
	joint_position[1] = joint_msg.position[1];
	joint_position[2] = joint_msg.position[2];
	joint_position[3] = joint_msg.position[3];
	joint_position[4] = joint_msg.position[4];
	joint_position[5] = joint_msg.position[5];
	joint_velocity = (joint_position - last_joint_position)*1000;
	last_joint_position = joint_position;
	//ROS_INFO_STREAM(""<<twist.twist.angular.x);
	//twist.twist.angular.x = joint_velocity[2];
	//twist.twist.angular.y = joint_velocity[1];
	//twist.twist.angular.z = joint_velocity[0];
}
//sireiti()
void master::pose_callback(const geometry_msgs::PoseStamped& pose_msg){
	posestamped.header.stamp =ros::Time::now();
	posestamped.pose.position.x = 0.001*(88.114+pose_msg.pose.position.z); 
    posestamped.pose.position.y = 0.001*pose_msg.pose.position.x;
    posestamped.pose.position.z = 0.001*(65.510+pose_msg.pose.position.y);
	posestamped.pose.orientation.x = -pose_msg.pose.orientation.z;
	posestamped.pose.orientation.y = -pose_msg.pose.orientation.x;
	posestamped.pose.orientation.z = pose_msg.pose.orientation.y;
	posestamped.pose.orientation.w = pose_msg.pose.orientation.w;
	pose_pub.publish(posestamped);
	
}


void master::button_callback(const geomagic_control::DeviceButtonEvent& button_msg){	
	
	if(button_msg.grey_button){
	button.data[0] = 0;
	}
	else{
	button.data[0] = 1;
	}

	//ROS_INFO_STREAM(""<<button.data[0]);
        //ROS_INFO_STREAM("gripper_angle_"<<gripper_angle_);
	if(button_msg.white_button){
	button.data[1] = 0;
	}
	else{
	button.data[1] = 1;
	}
	button_pub.publish(button);
}

void master::ur_callback(const geometry_msgs::WrenchStamped& ur_msg){
	force.force.x = ur_msg.wrench.force.y;
	force.force.y = -ur_msg.wrench.force.z;
	force.force.z = -ur_msg.wrench.force.x;
}
void master::init_force(){
	force.lock.push_back(0);//It doesn't work without this sentence.
}
//tikarafeedback
void master::set_force(){
	velocity_pub.publish(twist);
	force.force.x = 0;
	force.force.y = 0;
	force.force.z = 2;
	force_pub.publish(force);
}



int main(int argc, char **argv){
    ros::init(argc, argv, "geomagic_pub");
    ros::NodeHandle node_handle;
    ros::Rate loop_rate(500);
    master master(node_handle);
    master.init_force();
    while (ros::ok()){  
	master.set_force();  
    	ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}

#include <ros/ros.h>
#include <Eigen/Dense>
#include <cmath>
#include <eigen_conversions/eigen_kdl.h>
#include <eigen_conversions/eigen_msg.h>
#include "geomagic_control/DeviceFeedback.h"
using namespace Eigen;
using namespace std;
using namespace KDL;
namespace f_filters {
class IIR {
 public:
  // [n] Elements to filter
  // [m] Filter Order
  // [b_coef] Filter denominator (size: (m+1)x1)
  // [a_coed] Filter Numerator  (size: mx1)
  //  IIR(int n, int m, VectorXd b_coef, VectorXd a_coef);

  IIR(int n,  double r_coef)
      : N_elements_(n),r_coef_(r_coef) {
    Xvec_ = VectorXd::Zero(N_elements_);
    Yvec_ = VectorXd::Zero(N_elements_);
    flag_initialized = false;
  }

  ~IIR();

  VectorXd get_iir() {
    VectorXd iir = Xvec_.array() * r_coef_ + Yvec_.array() * (1- r_coef_);
    // for (int i = 0; i < N_elements_; i++) {
    //   iir[i] = el_mat_.row(i).sum() / order_;
    // }
    return iir;
  }


  void addX(VectorXd new_vec) {
    if (new_vec.size() == N_elements_) {
      Xvec_ = new_vec;
    } else {
      ROS_ERROR_STREAM(
          "filter: New vector dimentions doesnt match filter dimensions");
      return;
    }
    // ROS_INFO_STREAM(" Mat Filter: " << el_mat_);
  }

  void addY(VectorXd new_vec) {
    if (new_vec.size() == N_elements_) {
      Yvec_= new_vec;
    } else {
      ROS_ERROR_STREAM(
          "filter: New vector dimentions doesnt match filter dimensions");
      return;
    }
    // ROS_INFO_STREAM(" Mat Filter: " << el_mat_);
  }

  VectorXd update(VectorXd new_vec) {
    addX(new_vec);
    VectorXd out_filtered = get_iir();
    addY(out_filtered);

    return out_filtered;
  }

  //  VectorXd get_iir();
  //  void     shiftX(int n);
  //  void     shiftY(int n);
  //  void     init_full(VectorXd i_value);
  //  void     addX(VectorXd);
  //  void     addY(VectorXd);
  //  VectorXd update(VectorXd);

 private:
  int N_elements_;
  int order_;
  double r_coef_;
  double cut_off_freq;
  double sampling_rate;
  VectorXd Xvec_;
  VectorXd Yvec_;
  bool flag_initialized;
};  // class end

// ROS_INFO_STREAM(" Mat Initi: " << el_mat_);
} 

namespace integrators {
class integrators{
 public:
 
  integrators(){
  WI = Vector3d::Zero();
  WV = Vector3d::Zero();
  }
  ~integrators();
  
  Vector3d integrate(){
	Vector3d integral = WI.array() + WV.array()*0.002 ;
	return integral;
	}
  void addWV(Vector3d new_vec) {
	WV = new_vec;
	}
  void addWI(Vector3d new_vec) {
	WI = new_vec;
	}   
  Vector3d update(Vector3d new_vec) {
        addWV(new_vec);
        Vector3d WI = integrate();
        addWI(WI);
	return WI;
	}  
 private:
  Vector3d WV;
  Vector3d WI;
};
}

namespace master_control {

class master{
 public:   
  //Constructor
  master(ros::NodeHandle &node_handle);
  ~master();

  //Callback
  void geomagic_callback(const geometry_msgs::Twist& geo_msg);
  void ur_callback(const std_msgs::Float64MultiArray& u_s_);
  void ur_int_callback(const std_msgs::Float64MultiArray& u_s_trans);
  void pose_callback(const geometry_msgs::PoseStamped& pose_msg);
  void robot_pose_callback(const geometry_msgs::PoseStamped& robot_pose_msg);
  //Function
  void init_force();
  void set_force();

 private:
 //Nodehandle
 ros::NodeHandle n;
 //Subscriber
 ros::Subscriber geomagic_sub;
 ros::Subscriber ur_sub;
 ros::Subscriber pose_sub;
 ros::Subscriber robot_pose_sub;
 //Publisher
 ros::Publisher velocity_pub;
 ros::Publisher force_pub;
 ros::Publisher u_m_pub;
 ros::Publisher pose_pub;
 //Message
 geometry_msgs::TwistStamped twist;
 geometry_msgs::Pose pose;
 geomagic_control::DeviceFeedback force;
 std_msgs::Float64MultiArray u_m_trans;
 //Valiable
 double f_x, f_y,f_z;
 double b_;
 double cf;
 Eigen::Vector3d u_m_;
 Eigen::Vector3d v_m_;
 Eigen::Vector3d v_m_trans;
 Eigen::Vector3d v_m_int;
 Eigen::Vector3d u_m_int;
 Eigen::Vector3d v_m_tmp;
 Eigen::Vector3d robot_pose;
 //filter
 f_filters::IIR *wv_um_f_filter;
 f_filters::IIR *wv_vm_f_filter;
 //integral
 integrators::integrators *wv_um_integrator;
 integrators::integrators *wv_vm_integrator;
 };
}

#include <ros/ros.h>
#include <Eigen/Dense>
#include <cmath>
#include <eigen_conversions/eigen_kdl.h>
#include <eigen_conversions/eigen_msg.h>
#include <std_msgs/Float64.h>
#include "geomagic_control/DeviceFeedback.h"
#include <std_msgs/Header.h>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainiksolvervel_pinv.hpp>
#include <geometry_msgs/Vector3.h>
#include <control_msgs/JointTrajectoryControllerState.h>
using namespace Eigen;
using namespace std;
using namespace KDL;
namespace f_filters
{
  class IIR
  {
  public:
    // [n] Elements to filter
    // [m] Filter Order
    // [b_coef] Filter denominator (size: (m+1)x1)
    // [a_coed] Filter Numerator  (size: mx1)
    //  IIR(int n, int m, VectorXd b_coef, VectorXd a_coef);

    IIR(int n, double r_coef)
        : N_elements_(n), r_coef_(r_coef)
    {
      Xvec_ = VectorXd::Zero(N_elements_);
      Yvec_ = VectorXd::Zero(N_elements_);
      flag_initialized = false;
    }

    ~IIR();

    VectorXd get_iir()
    {
      VectorXd iir = Xvec_.array() * r_coef_ + Yvec_.array() * (1 - r_coef_);
      // for (int i = 0; i < N_elements_; i++) {
      //   iir[i] = el_mat_.row(i).sum() / order_;
      // }
      return iir;
    }

    void addX(VectorXd new_vec)
    {
      if (new_vec.size() == N_elements_)
      {
        Xvec_ = new_vec;
      }
      else
      {
        ROS_ERROR_STREAM(
            "filter: New vector dimentions doesnt match filter dimensions");
        return;
      }
      // ROS_INFO_STREAM(" Mat Filter: " << el_mat_);
    }

    void addY(VectorXd new_vec)
    {
      if (new_vec.size() == N_elements_)
      {
        Yvec_ = new_vec;
      }
      else
      {
        ROS_ERROR_STREAM(
            "filter: New vector dimentions doesnt match filter dimensions");
        return;
      }
      // ROS_INFO_STREAM(" Mat Filter: " << el_mat_);
    }

    VectorXd update(VectorXd new_vec)
    {
      addX(new_vec);
      VectorXd out_filtered = get_iir();
      addY(out_filtered);

      return out_filtered;
    }

    //  VectorXd get_iir();
    //  void     shiftX(int n);
    //  void     shiftY(int n);
    //  void     init_full(VectorXd i_value);
    //  void     addX(VectorXd);
    //  void     addY(VectorXd);
    //  VectorXd update(VectorXd);

  private:
    int N_elements_;
    int order_;
    double r_coef_;
    double cut_off_freq;
    double sampling_rate;
    VectorXd Xvec_;
    VectorXd Yvec_;
    bool flag_initialized;
  }; // class end

  // ROS_INFO_STREAM(" Mat Initi: " << el_mat_);
} // namespace f_filters

namespace integrators
{
  class integrators
  {
  public:
    integrators()
    {
      WI = Vector3d::Zero();
      WV = Vector3d::Zero();
      double dt, time, last_time;
    }
    ~integrators();

    Vector3d integrate()
    {
      time = ros::Time::now().toSec();
      dt = time - last_time;
      Vector3d integral = WI.array() + WV.array() * 0.002;
      return integral;
    }
    void addWV(Vector3d new_vec)
    {
      WV = new_vec;
    }
    void addWI(Vector3d new_vec)
    {
      WI = new_vec;
    }
    Vector3d update(Vector3d new_vec)
    {
      addWV(new_vec);
      Vector3d WI = integrate();
      addWI(WI);
      last_time = time;
      return WI;
    }

  private:
    Vector3d WV;
    Vector3d WI;
    double dt, time, last_time;
  };
} // namespace integrators

namespace master_control
{
  const double VEL_LIMIT_WAND = 0.05;
  const double DEG2RAD = 0.017453293;
  const double PI = 3.14159265359;
  //const double thread_sampling_time_sec_d_ = 0.002;
  const double thread_sampling_time_sec_d_ = 0.001;
  const double ANG_VEL_LIMIT_WAND = 0.4;
  class master
  {
  public:
    //Constructor
    master(ros::NodeHandle &node_handle);
    ~master();

    //Callback

    void geomagic_callback(const geometry_msgs::Twist &geo_msg);
    void ur_callback(const geometry_msgs::WrenchStamped &ur_msg);
    void ur_int_callback(const std_msgs::Float64MultiArray &u_s_trans);
    void pose_callback(const geometry_msgs::PoseStamped &pose_msg);
    void time_delay_callback(const std_msgs::Header &header_msg);
    void button_callback(const geomagic_control::DeviceButtonEvent &button_msg);
    void pinch_callback(const std_msgs::Float64 &pinch_msg);
    void joint_callback(const sensor_msgs::JointState &joint_msg);
    //fusano
    void HandPositionCallback(const geometry_msgs::PoseStamped &msg);
    void HandVelocityCallback(const geometry_msgs::Vector3 &msg);
    void RealAngleCallback(const control_msgs::JointTrajectoryControllerState &msg);
    void Oculus_nensei_Callback(const std_msgs::Float64 &msg);
    void master_position_Callback(const geometry_msgs::PoseStamped &msg);
    void slave_force_Callback(const geomagic_control::DeviceFeedback &msg);
    void slave_error_Callback(const geometry_msgs::Pose &msg);


    //Function
    void init_force();
    void set_force();
    std_msgs::Float64 HandPosition_Gain;
    geometry_msgs::Vector3 HandVelocity;
    std_msgs::Float64 C;
    geometry_msgs::TwistStamped twist;

  private:
    //Nodehandle
    ros::NodeHandle n;
    //Subscriber
    ros::Subscriber geomagic_sub;
    ros::Subscriber ur_sub;
    ros::Subscriber pose_sub;
    ros::Subscriber time_sub;
    ros::Subscriber button_sub;
    ros::Subscriber pinch_sub;
    ros::Subscriber joint_sub;
    //fusano
    ros::Subscriber HandVelocity_sub;
    ros::Subscriber HandPosition_sub;
    ros::Subscriber RealAngle_sub;
    ros::Subscriber Oculus_nensei_sub;
    ros::Subscriber master_position_sub;
    ros::Subscriber slave_force_sub;
    ros::Subscriber slave_error_sub;

    //Publisher
    ros::Publisher velocity_pub;
    ros::Publisher force_pub;
    ros::Publisher force2_pub;
    ros::Publisher u_m_pub;
    ros::Publisher pose_pub;
    ros::Publisher time_pub;
    ros::Publisher button_pub;
    ros::Publisher pinch_pub;
    ros::Publisher slave_error_pub;
    ros::Publisher slave_error_before_pub;
    
    //Message

    geometry_msgs::Pose pose;
    geometry_msgs::PoseStamped posestamped;
    std_msgs::Header time;
    geomagic_control::DeviceFeedback force;
    geomagic_control::DeviceFeedback ave_force;
    geomagic_control::DeviceFeedback pre_ave_force;
    geomagic_control::DeviceFeedback pre_filter_force;
    geomagic_control::DeviceFeedback force2;
    geomagic_control::DeviceFeedback slave_force;
    geometry_msgs::Pose slave_error;
    geometry_msgs::Pose ave_slave_error;
    geometry_msgs::Pose pre_ave_slave_error;
    geometry_msgs::Pose pre_filter_slave_error;
    geometry_msgs::Pose slave_error_before;
    std_msgs::Float64MultiArray u_m_;
    std_msgs::Float64MultiArray u_s_;
    std_msgs::Float64MultiArray v_m_;
    std_msgs::Float64MultiArray u_m_trans;
    std_msgs::Float64 gripper_angle_;
    std_msgs::Float64 realangle;

    std_msgs::Float64MultiArray button;

    //Valiable
    double f_x, f_y, f_z;
    double b_x_, b_y_, b_z_;
    double cf;
    double gripper_dif;
    double real_force_x;
    double filter_force_x;
    double prev_real_force_x;
    double prev_filter_force_x;
    double real_force_y;
    double filter_force_y;
    double prev_real_force_y;
    double prev_filter_force_y;
    double real_force_z;
    double filter_force_z;
    double prev_real_force_z;
    double prev_filter_force_z;
    double realposition_x;
    double realposition_y;
    double pre_white_button;
    double oculus_nensei;	
    Eigen::Vector3d v_m_tmp;
    Eigen::Vector3d ang_vel_tmp;
    Eigen::Vector3d v_m_trans;
    Eigen::Vector3d v_m_int;
    Eigen::Vector3d u_m_int;
    Eigen::VectorXd joint_position;
    Eigen::VectorXd joint_velocity;
    Eigen::VectorXd last_joint_position;
    Eigen::Quaterniond q_master;
    //fusano

  double past_position_error_x;
	double past_position_error_y;
	double past_position_error_z;
	double now_position_error_x;
	double now_position_error_y;
	double now_position_error_z;
	double KP_x;
	double KI_x;
	double KD_x;
	double KP_y;
	double KI_y;
	double KD_y;
	double KP_z;
	double KI_z;
	double KD_z;
	double DELTA_T;
	double integral_x;
	double integral_y;
	double integral_z;
	double objective_position_x;
	double objective_position_y;
	double objective_position_z;
	double time_integral;
	double Hz;
	double Hz_2;
        double slave_force_x;
        double slave_force_y;
        double slave_force_z;
        int data_size;
	double force_x[10];
	double force_y[10];
	double force_z[10];
	double force_result_x;
	double force_result_y;
	double force_result_z;
		
	


    //filter
    f_filters::IIR *wv_um_f_filter;
    f_filters::IIR *wv_vm_f_filter;
    f_filters::IIR *ang_vel_f_filter;
    //integral
    integrators::integrators *wv_um_integrator;
    integrators::integrators *wv_vm_integrator;
  };
} // namespace master_control
